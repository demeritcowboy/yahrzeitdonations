<?php

/**
 * Collection of upgrade steps.
 */
class CRM_Yahrzeitdonations_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * Add relationship field.
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_1100(): bool {
    $this->ctx->log->info('Applying update 1100');
    \Civi\Yahrzeitdonations\Installer::update1100();
    return TRUE;
  }

}

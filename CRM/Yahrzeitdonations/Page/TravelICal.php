<?php
use CRM_Yahrzeitdonations_ExtensionUtil as E;

class CRM_Yahrzeitdonations_Page_TravelICal extends CRM_Core_Page {

  public function run() {
    header('Cache-Control: no-store');
    header('Content-Type: text/calendar; charset=utf-8');
    header('Content-Disposition: attachment; filename=travel.ics');

    $template = CRM_Core_Smarty::singleton();
    $template->assign('now', gmdate('Ymd\THis\Z'));

    // The check for start_date not null seems silly but they appear to have
    // some of those.
    $dao = CRM_Core_DAO::executeQuery("SELECT e.id, e.start_date, e.end_date, e.title
      FROM civicrm_event e
      INNER JOIN civicrm_option_group og ON og.name = 'event_type'
      INNER JOIN civicrm_option_value ov ON (ov.option_group_id = og.id AND ov.name='Travel')
      WHERE e.is_active = 1
      AND (e.end_date IS NULL OR e.end_date >= %1)
      AND e.event_type_id = ov.value
      AND e.start_date IS NOT NULL",
      [1 => [date('Ym01000000'), 'Timestamp']]
    );
    $events = [];
    while($dao->fetch()) {
      $dtstart = new \DateTime($dao->start_date);
      if ($dao->end_date) {
        $dtend = new \DateTime($dao->end_date);
      }
      else {
        $dtend = new \DateTime($dao->start_date);
        // this is meaningless, but hopefully it means someone will notice and
        // fix it
        $dtend->modify('+11 months');
      }
      $events[] = [
        'eid' => $dao->id,
        'destination' => $dao->title,
        'url' => CRM_Utils_System::url('civicrm/event/manage/settings', 'reset=1&action=update&id=' . $dao->id, TRUE, NULL, FALSE),
        'dtstart' => $dtstart->format('Ymd'),
        'dtend' => $dtend->format('Ymd'),
        'uid' => sha1('travel:' . $dao->id),
      ];
    }

    $template->assign('events', $events);
    echo $template->fetch('CRM/Yahrzeitdonations/Page/TravelICal.tpl');

    CRM_Utils_System::civiExit();
  }

}

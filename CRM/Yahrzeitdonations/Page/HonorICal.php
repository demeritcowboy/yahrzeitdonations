<?php
use CRM_Yahrzeitdonations_ExtensionUtil as E;

class CRM_Yahrzeitdonations_Page_HonorICal extends CRM_Core_Page {

  public function run() {
    header('Cache-Control: no-store');
    header('Content-Type: text/calendar; charset=utf-8');
    header('Content-Disposition: attachment; filename=honor.ics');

    $template = CRM_Core_Smarty::singleton();
    $template->assign('now', gmdate('Ymd\THis\Z'));

    $results = \Civi\Api4\SearchDisplay::run(FALSE)
      ->setDisplay('Honor_Dedications_List')
      ->setSavedSearch('Honor_Dedications')
      ->execute();
    $events = [];
    foreach ($results as $r) {
      if (empty($r['data']['ContributionSoft_Contact_contact_id_01_Contact_Custom_Honoree_entity_id_01.Preferred_dedication_date'])) {
        // shouldn't happen but just in case
        continue;
      }
      $dt = new \DateTime($r['data']['ContributionSoft_Contact_contact_id_01_Contact_Custom_Honoree_entity_id_01.Preferred_dedication_date']);
      if ($dt->getTimestamp() < time()) {
        break;
      }
      $dtend = new \DateTime($r['data']['ContributionSoft_Contact_contact_id_01_Contact_Custom_Honoree_entity_id_01.Preferred_dedication_date']);
      $dtend->modify('+1 day');
      $events[] = [
        'cid' => $r['data']['ContributionSoft_Contact_contact_id_01.id'],
        'displayname' => $r['data']['ContributionSoft_Contact_contact_id_01.sort_name'],
        'ddisplayname' => $r['data']['ContributionSoft_Contribution_contribution_id_01_Contribution_Contact_contact_id_01.sort_name'],
        'url' => CRM_Utils_System::url('civicrm/contact/view', 'reset=1&selectedChild=custom_10&cid=' . $r['data']['ContributionSoft_Contact_contact_id_01.id'], TRUE, NULL, FALSE),
        'dtstart' => $dt->format('Ymd'),
        'dtend' => $dtend->format('Ymd'),
        'uid' => sha1('honor:' . $r['data']['id'] . ':' . $r['data']['ContributionSoft_Contact_contact_id_01.id'] . ':' . $dt->format('Ymd')) . '@torahinmotion.org',
        'done' => (bool) $r['data']['ContributionSoft_Contact_contact_id_01_Contact_Custom_Honoree_entity_id_01.Acknowledged_'],
      ];
    }

    $template->assign('events', $events);
    echo $template->fetch('CRM/Yahrzeitdonations/Page/HonorICal.tpl');

    CRM_Utils_System::civiExit();
  }

}

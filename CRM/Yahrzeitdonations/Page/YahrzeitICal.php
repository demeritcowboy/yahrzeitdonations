<?php
use CRM_Yahrzeitdonations_ExtensionUtil as E;

class CRM_Yahrzeitdonations_Page_YahrzeitICal extends CRM_Core_Page {

  public function run() {
    header('Cache-Control: no-store');
    header('Content-Type: text/calendar; charset=utf-8');
    header('Content-Disposition: attachment; filename=yahrzeit.ics');

    $template = CRM_Core_Smarty::singleton();
    $template->assign('now', gmdate('Ymd\THis\Z'));

    $jd = gregoriantojd(date('m'), date('d'), date('Y'));
    [$m, $d, $y] = explode('/', jdtojewish($jd));
    $events = [];
    for ($i = 1; $i <= 3; $i++) {
      $formValues = [
        'date_range_ui' => 'hebrew_month_year',
        'hebrew_year_choice' => $y,
        'hebrew_month_choice' => $m,
        'living_mourners' => 'only_living',
        'deceased_has_plaque' => 'all',
      ];
      $form = new CRM_Hebrewcalendarhelper_Form_Search_YahrzeitSearch($formValues);
      $dao = CRM_Core_DAO::executeQuery($form->all());
      while ($dao->fetch()) {
        if (empty($dao->yahrzeit_date_sort)) {
          // probably the before/after sunset hasn't been specified, so we don't know the date.
          continue;
        }

        $ydate = new \DateTime($dao->yahrzeit_date_sort);
        if ($ydate->getTimestamp() < time()) {
          continue;
        }

        // Try to get the most recent related contribution. There's nothing
        // robust to tell us that it was made for THIS year's yahrzeit, we just
        // have to guess based on how close the dates are.
        $contributionDao = CRM_Core_DAO::executeQuery("SELECT c.id, c.receive_date
          FROM civicrm_contribution_soft sft
          INNER JOIN civicrm_option_group og ON og.name = 'soft_credit_type'
          INNER JOIN civicrm_option_value ov ON (ov.option_group_id = og.id AND ov.name = 'in_memory_of')
          INNER JOIN civicrm_contribution c ON c.id = sft.contribution_id
          WHERE sft.contact_id = %1
          AND c.contact_id = %2
          ORDER BY c.receive_date DESC LIMIT 1",
          [
            1 => [$dao->deceased_contact_id, 'Integer'],
            2 => [$dao->contact_id, 'Integer'],
          ]
        );
        $is_recent = FALSE;
        $receive_date = E::ts('Unknown');
        if ($contributionDao->fetch()) {
          $receive_date = $contributionDao->receive_date;
          $cdate = new \DateTime($receive_date);
          $cdate->add(new \DateInterval('P2M'));
          // we added 2 months to the contribution date, so we say it's recent if it was in the 2 months before the yahrzeit date
          $is_recent = $cdate->getTimestamp() > $ydate->getTimestamp();
        }

        $dtend = new \DateTime($dao->yahrzeit_date_sort);
        $dtend->modify('+1 day');
        $events[] = [
          'cid' => $dao->contact_id,
          'displayname' => $dao->mourner_display_name,
          'dcid' => $dao->deceased_contact_id,
          'ddisplayname' => $dao->deceased_display_name,
          'url' => CRM_Utils_System::url('civicrm/contact/view', 'reset=1&cid=' . $dao->contact_id, TRUE, NULL, FALSE),
          'dtstart' => str_replace('-', '', $dao->yahrzeit_date_sort),
          'dtend' => $dtend->format('Ymd'),
          'uid' => sha1($dao->contact_id . ':' . $dao->deceased_contact_id . ':' . $dao->yahrzeit_date_sort) . '@torahinmotion.org',
          'is_recent' => $is_recent,
          'contribution_date' => $receive_date,
        ];
      }

      // Increment (hebrew) month, and if needed roll over to next year.
      // The outer loop is 3 times, so we end up doing this month and the next two
      $m++;
      if ($m > 13) {
        $m = 1;
        $y++;
      }
    }

    $template->assign('events', $events);
    echo $template->fetch('CRM/Yahrzeitdonations/Page/YahrzeitICal.tpl');

    CRM_Utils_System::civiExit();
  }

}

<?php
// This file declares a managed database record of type "ReportTemplate".
// The record will be automatically inserted, updated, or deleted from the
// database as appropriate. For more details, see "hook_civicrm_managed" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
return [
  [
    'name' => 'CRM_Yahrzeitdonations_Form_Report_YahrzeitContributions',
    'entity' => 'ReportTemplate',
    'params' => [
      'version' => 3,
      'label' => 'Yahrzeit Contributions',
      'description' => 'Yahrzeit Contributions, an extended soft credit report.',
      'class_name' => 'CRM_Yahrzeitdonations_Form_Report_YahrzeitContributions',
      'report_url' => 'yahrzeitdonations/yahrzeitcontributions',
      'component' => 'CiviContribute',
    ],
  ],
];

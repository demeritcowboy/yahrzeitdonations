# yahrzeitdonations

Adds custom fields for Hebrew Deceased Date which you can put in the Honoree profile for contribution forms so that In Memory Of donations automatically integrate with the Hebrew Calendar extension, creating the correct relationship and calculating an appropriate gregorian deceased date. This is useful when most of your donors only know the hebrew date of death or may not know the year, so the core deceased_date field is not useful to put on the form. But the Hebrew Calendar extension needs _a_ deceased_date, but for yahrzeit purposes it can be any of the anniversary dates that work out to the same hebrew month/day, which is what this extension puts in.

Note this requires the https://github.com/Chabadsuite/com.fountaintribe.hebrewcalendarhelper extension.

## Requirements

* PHP v7.2+
* CiviCRM (5.35+)

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl yahrzeitdonations@https://github.com/FIXME/yahrzeitdonations/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://github.com/FIXME/yahrzeitdonations.git
cv en yahrzeitdonations
```

## Getting Started

Add the Hebrew Deceased fields to the Honoree profile for donation pages where you offer In Memory Of donations.

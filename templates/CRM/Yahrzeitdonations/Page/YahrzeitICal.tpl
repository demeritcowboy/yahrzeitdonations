BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:Yahrzeits
X-WR-TIMEZONE:UTC
X-WR-CALDESC:Yahrzeits
{foreach from=$events item=e}
BEGIN:VEVENT
DTSTART;VALUE=DATE:{$e.dtstart}
DTEND;VALUE=DATE:{$e.dtend}
DTSTAMP:{$now}
UID:{$e.uid}
CLASS:PUBLIC
CREATED:{$now}
DESCRIPTION:{$e.url}\n
 Mourner: {$e.displayname}\n
 Deceased: {$e.ddisplayname}\n
 Contribution: {$e.contribution_date}
LAST-MODIFIED:{$now}
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:🕯{if $e.is_recent}📝{/if}{$e.displayname}
TRANSP:TRANSPARENT
END:VEVENT
{/foreach}
END:VCALENDAR

BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:Honor Dedications
X-WR-TIMEZONE:UTC
X-WR-CALDESC:Honor Dedications
{foreach from=$events item=e}
BEGIN:VEVENT
DTSTART;VALUE=DATE:{$e.dtstart}
DTEND;VALUE=DATE:{$e.dtend}
DTSTAMP:{$now}
UID:{$e.uid}
CLASS:PUBLIC
CREATED:{$now}
DESCRIPTION:{$e.url}\n
 Honoree: {$e.displayname}\n
 Donor: {$e.ddisplayname}
LAST-MODIFIED:{$now}
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:🎁{if !$e.done}📌{/if}{$e.ddisplayname}
TRANSP:TRANSPARENT
END:VEVENT
{/foreach}
END:VCALENDAR

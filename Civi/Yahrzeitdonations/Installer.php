<?php
namespace Civi\Yahrzeitdonations;

use CRM_Yahrzeitdonations_ExtensionUtil as E;

class Installer {

  public static function install() {
    $group_id = \Civi\Api4\CustomGroup::create(FALSE)
      ->setValues([
        'name' => 'memory_donations',
        'title' => E::ts('In Memory Donations'),
        'extends' => 'Individual',
        'style' => 'Inline',
        'collapse_display' => TRUE,
      ])
      ->execute()
      ->first()['id'];
    $days = [];
    for ($i = 1; $i <= 30; $i++) {
      $days[$i] = [
        'id' => $i,
        'label' => $i,
        'name' => "hebrew_day_$i",
      ];
    }
    \Civi\Api4\CustomField::create(FALSE)
      ->setValues([
        'custom_group_id' => $group_id,
        'name' => 'hebrew_deceased_day_of_month',
        'label' => E::ts('Hebrew Deceased Day of Month'),
        'data_type' => 'Int',
        'html_type' => 'Select',
        'option_values' => $days,
      ])
      ->execute();
    \Civi\Api4\CustomField::create(FALSE)
      ->setValues([
        'custom_group_id' => $group_id,
        'name' => 'hebrew_deceased_month',
        'label' => E::ts('Hebrew Deceased Month'),
        'data_type' => 'Int',
        'html_type' => 'Select',
        'option_values' => self::getHebrewMonthOptions(),
      ])
      ->execute();
    \Civi\Api4\CustomField::create(FALSE)
      ->setValues([
        'custom_group_id' => $group_id,
        'name' => 'hebrew_deceased_year',
        'label' => E::ts('Hebrew Deceased Year (leave blank if unknown)'),
        'data_type' => 'Int',
        'html_type' => 'Text',
      ])
      ->execute();

    self::update1100();
  }

  public static function uninstall() {
    // Delete option groups and values
    $custom_fields = \Civi\Api4\CustomField::get(FALSE)
      ->addSelect('option_group_id')
      ->addWhere('custom_group_id:name', '=', 'memory_donations')
      ->execute();
    foreach ($custom_fields as $field) {
      \Civi\Api4\OptionValue::delete(FALSE)
        ->addWhere('option_group_id', '=', $field['option_group_id'])
        ->execute();
      \Civi\Api4\OptionGroup::delete(FALSE)
        ->addWhere('id', '=', $field['option_group_id'])
        ->execute();
    }

    // Delete custom fields
    \Civi\Api4\CustomField::delete(FALSE)
      ->addWhere('custom_group_id:name', '=', 'memory_donations')
      ->execute();
    // Delete custom group
    \Civi\Api4\CustomGroup::delete(FALSE)
      ->addWhere('name', '=', 'memory_donations')
      ->execute();
  }

  /**
   * Called from both the install and upgrader.
   */
  public static function update1100() {
    $custom_group = \Civi\Api4\CustomGroup::get(FALSE)
      ->addWhere('name', '=', 'memory_donations')
      ->addSelect('id')
      ->execute()->first();

    \Civi\Api4\CustomField::create(FALSE)
      ->setValues([
        'custom_group_id' => $custom_group['id'],
        'name' => 'your_relation',
        'label' => E::ts('Your relation to Deceased'),
        'data_type' => 'String',
        'html_type' => 'Select',
        'option_values' => self::getRelationshipOptions(),
      ])
      ->execute();
  }

  /**
   * Get the hebrew months list in a format suitable for the api call.
   *
   * Note the values (oddly called id here) match up to the php
   * function jewishtojd() which takes 1-13 as a month value.
   *
   * @return array
   */
  private static function getHebrewMonthOptions(): array {
    return [
      1 => [
        'id' => 1,
        'label' => E::ts('Tishrei'),
        'name' => 'hebrew_month_1',
      ],
      2 => [
        'id' => 2,
        'label' => E::ts('Heshvan'),
        'name' => 'hebrew_month_2',
      ],
      3 => [
        'id' => 3,
        'label' => E::ts('Kislev'),
        'name' => 'hebrew_month_3',
      ],
      4 => [
        'id' => 4,
        'label' => E::ts('Tevet'),
        'name' => 'hebrew_month_4',
      ],
      5 => [
        'id' => 5,
        'label' => E::ts('Shevat'),
        'name' => 'hebrew_month_5',
      ],
      6 => [
        'id' => 6,
        'label' => E::ts('Adar'),
        'name' => 'hebrew_month_6',
      ],
      7 => [
        'id' => 7,
        'label' => E::ts('Adar II'),
        'name' => 'hebrew_month_7',
      ],
      8 => [
        'id' => 8,
        'label' => E::ts('Nisan'),
        'name' => 'hebrew_month_8',
      ],
      9 => [
        'id' => 9,
        'label' => E::ts('Iyyar'),
        'name' => 'hebrew_month_9',
      ],
      10 => [
        'id' => 10,
        'label' => E::ts('Sivan'),
        'name' => 'hebrew_month_10',
      ],
      11 => [
        'id' => 11,
        'label' => E::ts('Tamuz'),
        'name' => 'hebrew_month_11',
      ],
      12 => [
        'id' => 12,
        'label' => E::ts('Av'),
        'name' => 'hebrew_month_12',
      ],
      13 => [
        'id' => 13,
        'label' => E::ts('Elul'),
        'name' => 'hebrew_month_13',
      ],
    ];
  }

  /**
   * We don't include all possible ones, and we want to reword a bit.
   * @return array
   */
  private static function getRelationshipOptions(): array {
    return [
      // hardcoding these for now
      '1-a_b' => [
        'id' => '1-a_b',
        'label' => E::ts('I am their Child'),
        'name' => 'your_relationship1_a_b',
      ],
      '2-a_b' => [
        'id' => '2-a_b',
        'label' => E::ts('I am their Spouse'),
        'name' => 'your_relationship2_a_b',
      ],
      '3-a_b' => [
        'id' => '3-a_b',
        'label' => E::ts('I am their Sibling'),
        'name' => 'your_relationship3_a_b',
      ],
      '14-a_b' => [
        'id' => '14-a_b',
        'label' => E::ts('I am their Partner'),
        'name' => 'your_relationship14_a_b',
      ],
      '1-b_a' => [
        'id' => '1-b_a',
        'label' => E::ts('I am their Parent'),
        'name' => 'your_relationship1_b_a',
      ],
      '17-a_b' => [
        'id' => '17-a_b',
        'label' => E::ts('Other'),
        'name' => 'your_relationship17_a_b',
      ],
    ];
  }

}

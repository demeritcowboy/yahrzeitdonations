<?php

require_once 'yahrzeitdonations.civix.php';
use CRM_Yahrzeitdonations_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function yahrzeitdonations_civicrm_config(&$config) {
  _yahrzeitdonations_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function yahrzeitdonations_civicrm_install() {
  _yahrzeitdonations_civix_civicrm_install();
  \Civi\Yahrzeitdonations\Installer::install();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function yahrzeitdonations_civicrm_uninstall() {
  \Civi\Yahrzeitdonations\Installer::uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function yahrzeitdonations_civicrm_enable() {
  _yahrzeitdonations_civix_civicrm_enable();
}

function yahrzeitdonations_civicrm_buildForm($formName, $form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    // Dedication date field should only show for honor of.
    $field = \Civi\Api4\CustomField::get(FALSE)
      ->addSelect('id')
      ->addWhere('custom_group_id:name', '=', 'Honoree')
      ->addWhere('name', '=', 'Preferred_dedication_date')
      ->execute()->first();
    $dedicationDateField = $field['id'] ?? '';

    // Only show the memory fields when In Memory Of is selected
    $memory = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_ContributionSoft', 'soft_credit_type_id', 'in_memory_of');
    $f = [];
    $custom_fields = \Civi\Api4\CustomField::get(FALSE)
      ->addSelect('id')
      ->addWhere('custom_group_id:name', '=', 'memory_donations')
      ->execute();
    foreach ($custom_fields as $field) {
      $f[] = $field['id'];
    }
    $fieldjson = json_encode($f);
    $js = <<<ENDJS
(function($) {
  $('input[name="soft_credit_type_id"]').click(function() {
    if ($(this).val() == '{$memory}') {
      // Sometimes it has honor and sometimes it doesn't (?)
      {$fieldjson}.forEach(i => $('#editrow-honor_custom_' + i).show());
      {$fieldjson}.forEach(i => $('#editrow-custom_' + i).show());
      $('#editrow-honor_custom_{$dedicationDateField}').hide();
      $('#editrow-custom_{$dedicationDateField}').hide();
    } else {
      {$fieldjson}.forEach(i => $('#editrow-honor_custom_' + i).hide());
      {$fieldjson}.forEach(i => $('#editrow-custom_' + i).hide());
      $('#editrow-honor_custom_{$dedicationDateField}').show();
      $('#editrow-custom_{$dedicationDateField}').show();
    }
  });
})(CRM.$)
ENDJS;
    \Civi::resources()->addScript($js);
  }
}

/**
 * Implements hook_civicrm_post().
 */
function yahrzeitdonations_civicrm_post($op, $objectName, $objectId, &$objectRef) {
  if ($objectName == 'ContributionSoft' && $op == 'create') {
    // Only process In Memory Of
    if ($objectRef->soft_credit_type_id == CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_ContributionSoft', 'soft_credit_type_id', 'in_memory_of')) {
      // Retrieve contribution
      $contribution = civicrm_api3('Contribution', 'getsingle', ['id' => $objectRef->contribution_id, 'return' => ['contact_id']]);
      // Need the custom fields on the deceased contact
      $deceased = \Civi\Api4\Contact::get(FALSE)
        ->addWhere('id', '=', $objectRef->contact_id)
        ->addSelect(
          'memory_donations.hebrew_deceased_month',
          'memory_donations.hebrew_deceased_day_of_month',
          'memory_donations.hebrew_deceased_year',
          'memory_donations.your_relation'
        )
        ->execute()->first();

      // Don't create same relationship twice
      // This might not work if civi doesn't recognize the soft-credit contact
      // as a duplicate, e.g. if they don't enter an email for it.
      $exists = civicrm_api3('Relationship', 'get', [
        'relationship_type_id' => 'Yahrzeit_observed_by',
        'contact_id_a' => $objectRef->contact_id,
        'contact_id_b' => $contribution['contact_id'],
      ]);
      if ($exists['count'] == 0) {
        // Create mourner relationship
        civicrm_api3('Relationship', 'create', [
          'relationship_type_id' => 'Yahrzeit_observed_by',
          'contact_id_a' => $objectRef->contact_id,
          'contact_id_b' => $contribution['contact_id'],
        ]);
        if (!empty($deceased['memory_donations.your_relation'])) {
          $rel_id = explode('-', $deceased['memory_donations.your_relation']);
          // Check if it's a-to-b or b-to-a
          if ($rel_id[1] == 'a_b') {
            civicrm_api3('Relationship', 'create', [
              'relationship_type_id' => $rel_id[0],
              'contact_id_a' => $contribution['contact_id'],
              'contact_id_b' => $objectRef->contact_id,
            ]);
          }
          else {
            civicrm_api3('Relationship', 'create', [
              'relationship_type_id' => $rel_id[0],
              'contact_id_a' => $objectRef->contact_id,
              'contact_id_b' => $contribution['contact_id'],
            ]);
          }
        }
      }

      if (!empty($deceased['memory_donations.hebrew_deceased_month']) && !empty($deceased['memory_donations.hebrew_deceased_day_of_month'])) {
        // Note the arbitrary year - it's irrelevant for calculating
        // future yahrzeit dates but means the deceased_date in civi will
        // rarely be the real year, but often they don't know or remember the
        // year anyway, and only staff see it.
        $year = 5781;
        if (!empty($deceased['memory_donations.hebrew_deceased_year'])) {
          $year = $deceased['memory_donations.hebrew_deceased_year'];
          if (!is_numeric($year) || $year < 5000 || $year > 9999) {
            // nonsense
            $year = 5781;
          }
        }
        // This assumes the month values have an additional Adar II with value
        // 7, otherwise we'd need to add 1 if the value is greater than 6 since
        // the function takes a number 1 to 13 expecting Adar to be both 6 and
        // 7.
        $jd = jewishtojd($deceased['memory_donations.hebrew_deceased_month'], $deceased['memory_donations.hebrew_deceased_day_of_month'], $year);
        $dt = DateTime::createFromFormat('m/d/Y', jdtogregorian($jd));
        \Civi\Api4\Contact::update(FALSE)
          ->addValue('deceased_date', $dt->format('Y-m-d'))
          ->addValue('is_deceased', TRUE)
          ->addValue('Extended_Date_Information.Death_Date_Before_Sunset', 1)
          ->addWhere('id', '=', $objectRef->contact_id)
          ->execute();
        civicrm_api3('AllHebrewDates', 'calculate', [
          'contact_ids' => $objectRef->contact_id,
        ]);
      }
    }
  }
}
